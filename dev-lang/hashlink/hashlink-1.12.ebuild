# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Copied/adapted from the ebuild dev-lang/neko

EAPI=8

inherit flag-o-matic cmake

DESCRIPTION="A virtual machine for Haxe"
HOMEPAGE="https://hashlink.haxe.org/
	https://github.com/HaxeFoundation/hashlink/"

if [[ "${PV}" == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/HaxeFoundation/${PN}.git"
else
	SRC_URI="https://github.com/HaxeFoundation/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
	S="${WORKDIR}/${PN}-${PV}"
fi

LICENSE="MIT"
SLOT="0/${PV}"
IUSE="sqlite ssl sdl openal"

PATCHES=(
	"${FILESDIR}/0001-fix-for-cmake-on-non-windows-machines.patch"
)

RDEPEND="
	media-libs/libpng
	media-libs/libjpeg-turbo
	media-libs/libvorbis
	dev-libs/libuv
	ssl? ( net-libs/mbedtls:= )
	sqlite? ( dev-db/sqlite:3= )
	sdl? ( media-libs/libsdl2:= )
	openal? ( media-libs/openal:= )
"
DEPEND="${RDEPEND}"

pkg_setup() {
	# It is necessary to do this because hashlink uses haxelib
	haxelib setup "${T}/haxelib-tmp"
	haxelib install hashlink
	return
}

src_configure() {
	# -Werror=strict-aliasing warnings, bug #855641
	filter-lto
	append-flags -fno-strict-aliasing

	#local mycmakeargs=(
	#	-DRUN_LDCONFIG=OFF
	#	-DWITH_NEKOML=ON
	#	-DWITH_REGEXP=ON
	#	-DWITH_UI=OFF
	#	-DWITH_APACHE=$(usex apache)
	#	-DWITH_MYSQL=$(usex mysql)
	#	-DWITH_SQLITE=$(usex sqlite)
	#	-DWITH_SSL=$(usex ssl)
	#)
	local mycmakeargs=(
		-DWITH_UI=$(usex sdl)
		-WDITH_VIDEO=$(usex sdl)
		-DWITH_SSL=$(usex ssl)
		-DWITH_SQLITE=$(usex sqlite)
		-DWITH_OPENAL=$(usex openal)
	)
	cmake_src_configure
}

# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=(python3_{7..11})
inherit distutils-r1 git-r3

DESCRIPTION="Mutt and terminal url selector (similar to urlview)"
HOMEPAGE="https://github.com/firecat53/urlscan"
EGIT_REPO_URI="https://github.com/firecat53/urlscan"

if [[ ${PV} == *9999 ]];then
	EGIT_COMMIT="HEAD"
else
	EGIT_COMMIT="${PV}"
fi

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	${RDEPEND}
"
RDEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	>=dev-python/urwid-1.2:=
"
